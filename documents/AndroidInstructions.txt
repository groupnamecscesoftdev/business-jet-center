BJ's Dev platform instructions for android.

1. Install the Java SDK from HERE: http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
	You're specifically looking for jdk-7u51-windows-i586.exe
2. Once installation is complete open up control panel -> system -> Advanced System Settings -> (tab) Advanced -> Environment Variables
	
	Under System Variables click [New]
	Variable Name : JAVA_HOME
	Variable Value: [Your JDK Directory... Example : C:\Program Files (x86)\Java\jdk1.7.0_51]
	
	OK
3. Under system Variables scroll to path and add ;%JAVA_HOME%\bin to the end of your path.

	OK
	OK
	OK

4. Download ant here: http://download.nextag.com/apache//ant/binaries/apache-ant-1.9.3-bin.zip

	Copy ant to a directory. I'm putting most of my dev tools in a single folder called dev. ie: C:\dev\ant for ant
	
	Go back to your environment variables (see step 2)
	
	Under System Variables click [New]
	Variable Name : ANT_HOME
	Variable Value: [Your ANT Directory... Example : C:\dev\ant]
	
	OK
	
5. Under system Variables scroll to path and add ;%ANT_HOME%\bin to the end of your path.

	OK
	OK
	OK
	
6. download and install android dev studio from here: http://developer.android.com/sdk/installing/studio.html

7. Run Android Dev studio
8. Click the arrow next to configure
9. Open the sdk Manager
	Click all the android 4.* and also 2.2 so they are selected with checkboxes
	Click install packages.
	Accept all the TOSs etc...
	
	Install all dat stuff. it'll take a while.

10. Open up your environment variables again (see step 2)
	Under system Variables scroll to path and add the path to the android development studio sub directories for both tools and platform tools, for example mine looks like this.
	;C:\dev\Android\android-studio\sdk\platform-tools;C:\dev\Android\android-studio\sdk\tools
	
Now follow the instruction to install phonegap here: http://docs.phonegap.com/en/edge/guide_cli_index.md.html#The%20Command-Line%20Interface

Note that you'll need to install nodejs (hint any commands that call for sudo should just be run in an admin command prompt in windows without sudo in front of the command).

Then follow the android platform guide here: http://docs.phonegap.com/en/edge/guide_platforms_android_index.md.html#Android%20Platform%20Guide

11. You'll need to set up some AVDs in order to run the emulator from the command line. To do that open up a command prompt and navigate to the sdk/tools subdirectory of your android dev studio install.
	run the command: android avd
	
	Make a few avds

	You'll need to start an emulator ahead of time but once you do you can then use phonegap to run the emulator
	

	